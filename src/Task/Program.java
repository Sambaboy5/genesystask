package Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Program {

	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter value :");
		int number = scan.nextInt();
		String temp = Integer.toString(number);
		int[] newNumbers = new int[temp.length()];
		if (temp.length()<=0 || temp.length()>=5) {
			System.out.println("Please enter 1-4 digits number");
			System.exit(0);
		}
		for (int i = 0; i < temp.length(); i++)
		{
			newNumbers[i] = temp.charAt(i) - '0';
		}
		for (int i = 0; i < newNumbers.length; i++) {
			if (newNumbers[i]==0 || newNumbers[i]==1) {
				System.out.println("Please enter 2-9 digits number");
				System.exit(0);
			}
		}
		
		letterCombinations(newNumbers, temp.length());
	}

	public static ArrayList<String> letterCombinationsUtil(int[] numbers, int n, String[] table) {
		ArrayList<String> list = new ArrayList<>();
		Queue<String> queue = new LinkedList<>();
		
		queue.add("");
		
		while (!queue.isEmpty()) {
			String s = queue.remove();
			if (s.length() == n)
				list.add(s);
			else {
				String val = table[numbers[s.length()]];
				for (int i = 0; i < val.length(); i++) {
					queue.add(s + val.charAt(i));
				}
			}
		}
		return list;
	}

	static void letterCombinations(int[] numbers, int n) {
		String[] table = { "_", " ", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };
		

		ArrayList<String> list = letterCombinationsUtil(numbers, n, table);

		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i) + " ");
		}
	}

}
