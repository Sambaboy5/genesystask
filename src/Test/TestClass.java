package Test;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;

import Task.Program;

public class TestClass {
	
	@Test
	public void exampleOne() {
		String[] table = {"_", " ", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
		int[] numbers = {2, 2};
		
		List<String> expected = List.of("aa", "ab", "ac", "ba", "bb", "bc", "ca", "cb", "cc");
		ArrayList<String> current = Program.letterCombinationsUtil(numbers, numbers.length, table);
		
		assertArrayEquals(expected.toArray(), current.toArray());
	}
	
	@Test
	public void exampleTwo() {
		String[] table = {"_", " ", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
		int[] numbers = {2};
		
		List<String> expected = List.of("a", "b", "c");
		ArrayList<String> current = Program.letterCombinationsUtil(numbers, numbers.length, table);
		
		assertArrayEquals(expected.toArray(), current.toArray());
	}
	
}
